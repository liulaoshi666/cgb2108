package com.jt.config;

import com.jt.pojo.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 需求: 自定义User对象,交给Spring容器管理
 * 注解讲解:
 *      1.@Configuration 该类是一个配置类
 *         理解:该类是一块区域,在区域中编辑自定义对象
 */
@Configuration
public class UserConfig {
    /**
     * 要求: 必须有返回值.
     * 功能: 被@Bean修饰的方法,将方法名当做key--user,将返回值对象当做值value
     *       根据Key-value 存储到Map集合中 交给Spring容器管理
     *       Map<user,User对象>
     * @return
     */
    @Bean
    public User user(){
        User user = new User();
        user.setId(100).setName("tomcat猫").setSex("男").setAge(18);
        return user;
    }
}
