package com.jt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
//注解说明: @RestController = @Controller + @ResponseBody
//效果:  1. @Controller 将当前类交给Spring容器管理
//      2. @ResponseBody 前后端交互时,将后端服务器返回的对象转化为JSON
//         前后端交互媒介 http协议 传输的数据都是字符串
//         JSON: 有特殊格式的字符串

@RestController
//加载指定配置文件,并且设定字符集编码格式
@PropertySource(value = "classpath:/name.properties",encoding = "UTF-8")
public class HelloController {

    //作用:从Spring容器中获取数据,需要指定key
    //     $ springel   spel表达式
    @Value("${cgbname}")
    private String name;    // = "王五";

    @Value("${cgbname2}")   //该属性是pro文件中,必须要求spring容器加载pro文件
    private String name2;

    /**
     * URL: http://localhost:8080/getName
     * 返回值: 变量的name属性
     */
    @RequestMapping("/getName") //建议大家写绝对路径
    public String getName(){

        return name+"|"+name2;
    }
}
