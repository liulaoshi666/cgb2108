package com.jt.controller;

import com.jt.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//需求: 测试User对象是否交给Spring容器管理
@RestController
public class UserController {
    //1.先管理对象,之后再注入对象
    @Autowired
    private User user;

    @RequestMapping("/getUser")
    public User getUser(){
        return user;
    }
}
