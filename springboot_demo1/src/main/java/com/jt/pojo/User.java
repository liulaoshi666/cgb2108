package com.jt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * POJO写法说明:
 *  1.必须有get/set方法
 *  2.必须实现序列化接口 数据流传输/socket通讯
 *
 * 注解说明:
 * @Data 自动生成get/set/toString/equals/hashcode等方法
 * @Accessors(chain = true)  重写了Set方法..
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor  //无参构造
@AllArgsConstructor //全参构造
public class User implements Serializable {
    private Integer id;
    private String name;
    private Integer age;
    private String sex;

   /* public void setId(Integer id){
        this.id = id;
    }

    public User setId(Integer id){
        this.id = id;
        //this 代表当前对象 运行期有效
        return this;
    }*/
}
