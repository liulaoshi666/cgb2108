package com.jt.mapper;

import com.jt.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface UserAnnoMapper {

    /**
     * 注解使用规则:
     *  1.注解标识接口方法. 接口方法调用,直接注解的内容.
     *  2.注解将查询的结果集,根据方法的返回值类型动态映射.
     */
    //查询user表的数据记录
    @Select("select * from demo_user")
    //新增 @Insert("sql")
    //修改 @Update("sql")
    //删除 @Delete("sql")
    List<User> findAll();

    @Insert("insert into demo_user(id,name,age,sex) " +
            "value(null,#{name},#{age},#{sex})")
    int saveUser(User user);
}
