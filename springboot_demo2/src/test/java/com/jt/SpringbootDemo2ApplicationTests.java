package com.jt;

import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class SpringbootDemo2ApplicationTests {

    private SqlSessionFactory sqlSessionFactory;

    /**
     * 设计思路:
     *   1.要求每次执行测试方法时 都先生成一个SqlSessionFactory
     *   2.从sqlSessionFactory中获取sqlSession
     *   3.完成业务操作
     *   注解说明:
     *      @BeforeEach 当每次执行@Test注解方法时,都会先执行该方法.
     */
    @BeforeEach
    public void init() throws IOException {
        String resource = "mybatis/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

    }

    @Test
    public void testFind2(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        List<User> userList = userMapper.findAll();
        System.out.println(userList);
        sqlSession.close();
    }

    
    /**
     * 核心问题: SqlSession 理解为Mybatis操作数据的连接
     * 1. 通过 SqlSessionFactoryBuilder 构建SqlSessionFactory
     */

    @Test
    public void testMybatis01() throws IOException {
        //1.定义核心配置文件的路径
        String resource = "mybatis/mybatis-config.xml";
        //2.通过工具API读取资源文件
        InputStream inputStream = Resources.getResourceAsStream(resource);
        //3.构建SqlSessionFactory
        SqlSessionFactory sqlSessionFactory =
                    new SqlSessionFactoryBuilder().build(inputStream);
        //4.获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //5.获取Mapper的接口对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        //6.调用接口方法 获取返回值数据
        List<User> userList = userMapper.findAll();
        System.out.println(userList);
        //7.连接用完记得关闭
        sqlSession.close();
        inputStream.close();
    }


    //根据Id-查询数据库
    @Test
    public void testFindUserById(){
        int id = 1;
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        User user = userMapper.findUserById(id);
        System.out.println(user);
    }

    //实现User对象的新增操作
    //mybatis在执行"更新"操作时 需要提交事务
    @Test
    public void testSaveUser(){
        User user = new User(null,"嫦娥",16, "女");
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        int rows = userMapper.saveUser(user);
        System.out.println("影响的行数:"+rows);
        //提交事务
        sqlSession.commit();
        sqlSession.close();
    }

    /**
     *  需求:  要求将id=232的数据 name="嫦娥姐姐" age=99岁
     */
    @Test
    public void testUpdateUser(){
        //1.封装数据
        User user = new User(232,"嫦娥姐姐",99,null);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        int rows = userMapper.updateUser(user);
        if(rows>0){
            //事务提交
            sqlSession.commit();
        }
        System.out.println("影响了:" + rows+"行");
        sqlSession.close();
    }

    /**
     *  完成删除操作  将name="嫦娥姐姐"的数据删除.
     *  sql: delete from demo_user where name=#{嫦娥姐姐}
     */
    @Test
    public void deleteUserByName(){
        //获取SqlSession,之后自动提交事务
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        String name = "嫦娥姐姐";
        int rows = userMapper.deleteUserByName(name);
        sqlSession.close();
    }

    //需求:  查询 age> 100 并且  age < 1000的用户.
    //知识点: 如果有重名属性,一般使用Map集合封装数据
    @Test
    public void testMap(){
        //获取SqlSession,之后自动提交事务
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        Map map = new HashMap();
        map.put("minAge",100);
        map.put("maxAge",1000);
        List<User> userList = userMapper.findByAge(map);
        System.out.println(userList);
        sqlSession.close();
    }

    /**
     * 测试@Param注解
     * 需求:  查询 age> 100 并且  age < 1000的用户.
     */
    @Test
    public void testParam(){
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        int minAge =  100;
        int maxAge =  1000;
        List<User> userList = userMapper.findParam(minAge,maxAge);
        System.out.println(userList);
        sqlSession.close();
    }

    /**
     * 查询name中包含"乔"字的用户
     */
    @Test
    public void testLike(){
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        String name = "乔";
        List<User> userList = userMapper.findUserByLike(name);
        System.out.println(userList);
        sqlSession.close();
    }

















}
