package com.jt;

import com.jt.mapper.UserAnnoMapper;
import com.jt.mapper.UserMapper2;
import com.jt.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

public class TestAnno {

    private SqlSessionFactory sqlSessionFactory;

    @BeforeEach
    public void init() throws IOException {
        String resource = "mybatis/mybatis-config.xml";
        InputStream inputStream =
                Resources.getResourceAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder()
                    .build(inputStream);
    }

    //测试注解功能
    @Test
    public void testAnno(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserAnnoMapper userAnnoMapper =
                        sqlSession.getMapper(UserAnnoMapper.class);
        List<User> userList = userAnnoMapper.findAll();
        System.out.println(userList);
        sqlSession.close();
    }

    //注解实现新增
    @Test
    public void testAnnoSave(){
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        UserAnnoMapper userAnnoMapper =
                sqlSession.getMapper(UserAnnoMapper.class);
        User user = new User(null,"注解测试",18,"男");
        int rows = userAnnoMapper.saveUser(user);
        System.out.println("新增入库成功!!!");
        sqlSession.close();
    }
}
