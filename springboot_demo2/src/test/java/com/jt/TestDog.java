package com.jt;

import com.jt.mapper.DogMapper;
import com.jt.pojo.Dog;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class TestDog {

    private SqlSessionFactory sqlSessionFactory;

    @BeforeEach
    public void init() throws IOException {
        String resource = "mybatis/mybatis-config.xml";
        InputStream inputStream =
                Resources.getResourceAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder()
                .build(inputStream);
    }

    //查询所有dog表的数据
    @Test
    public void testResultMap(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        DogMapper dogMapper = sqlSession.getMapper(DogMapper.class);
        List<Dog> dogList = dogMapper.findAll();
        System.out.println(dogList);
        sqlSession.close();
    }

}
