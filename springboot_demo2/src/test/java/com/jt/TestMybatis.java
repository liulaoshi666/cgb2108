package com.jt;

import com.jt.mapper.UserMapper2;
import com.jt.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.*;

public class TestMybatis {

    private SqlSessionFactory sqlSessionFactory;

    @BeforeEach
    public void init() throws IOException {
        String resource = "mybatis/mybatis-config.xml";
        InputStream inputStream =
                Resources.getResourceAsStream(resource);
        sqlSessionFactory = new SqlSessionFactoryBuilder()
                    .build(inputStream);
    }

    //查询用户所有数据
    @Test
    public void findUser(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper2 userMapper2 = sqlSession.getMapper(UserMapper2.class);
        List<User> userList = userMapper2.findUser();
        System.out.println(userList);
        sqlSession.close();
    }

    /**
     * 业务:  查询id号 1,2,4,5,7的数据
     */
    @Test
    public void testFindIn(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper2 userMapper2 = sqlSession.getMapper(UserMapper2.class);
        //将数据封装为数组
        int[] ids = {1,2,4,5,7};
        List<User> userList = userMapper2.findIn(ids);
        System.out.println(userList);
        sqlSession.close();
    }

    /**
     * 业务:  查询id号 1,2,4,5,7的数据
     * 知识点: 数组转化时,需要使用包装类型.
     * 根源:  基本类型没有get/set方法
     *       包装类型是对象 对象中有方法
     */
    @Test
    public void testFindInList(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper2 userMapper2 = sqlSession.getMapper(UserMapper2.class);
        //将数组转化为List集合
        Integer[] ids = {1,2,4,5,7};
        List list = Arrays.asList(ids);
        List<User> userList = userMapper2.findInList(list);
        System.out.println(userList);
        sqlSession.close();
    }

    /**
     *  需求：
     *      查询id=1,3,5,6,7 并且sex="男"的用户
     *  Sql:
     *      select * from demo_user where id in (1,3....)
     *      and sex = "男"
     */
    @Test
    public void testFindInMap(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper2 userMapper2 = sqlSession.getMapper(UserMapper2.class);
        int[] ids = {1,3,5,6,7};
        String sex = "男";
       /* Map map = new HashMap();
        map.put("ids",ids);
        map.put("sex",sex);*/
        List<User> userList = userMapper2.findInMap(ids,sex);
        System.out.println(userList);
        sqlSession.close();
    }

    /**
     * 动态Sql练习  根据对象中不为null的元素查询数据
     */
    @Test
    public void testSqlWhere(){
        User user = new User(1,"黑熊精",null, "男");
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserMapper2 userMapper2 = sqlSession.getMapper(UserMapper2.class);
        List<User> userList = userMapper2.findSqlWhere(user);
        System.out.println(userList);
        sqlSession.close();
    }

    /**
     * 需求: 实现用户数据修改, 根据对象中不为null的数据完成修改操作
     */
    @Test
    public void testSqlSet(){
        SqlSession sqlSession =
                sqlSessionFactory.openSession(true);
        UserMapper2 userMapper = sqlSession.getMapper(UserMapper2.class);
        User user = new User(1,"守山使者",3000, null);
        int rows = userMapper.updateSqlSet(user);
        System.out.println("影响"+rows+"行");
        sqlSession.close();
    }

    @Test
    public void testChoose(){
        SqlSession sqlSession =
                sqlSessionFactory.openSession();
        UserMapper2 userMapper = sqlSession.getMapper(UserMapper2.class);
        User user = new User(null,null,null,"男");
        List<User> userList = userMapper.findChoose(user);
        System.out.println(userList);
        sqlSession.close();
    }
}
