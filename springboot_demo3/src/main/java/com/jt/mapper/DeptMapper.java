package com.jt.mapper;

import com.jt.pojo.Dept;

import java.util.List;

public interface DeptMapper {
    List<Dept> findAll();
    //实现数据子查询
    List<Dept> selectChildren();
}
