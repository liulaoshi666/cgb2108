package com.jt.mapper;

import com.jt.pojo.Emp;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface EmpMapper {

    List<Emp> findAll();

    List<Emp> findAllSelect();

    List<Emp> getAll();
}
