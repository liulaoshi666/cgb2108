package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController  //@ResponseBody 将list集合转化为json
@CrossOrigin     //解决跨域问题的
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * URL: http://localhost:8090/findAll
     * 参数: 无
     * 返回值: List<User>
     */
    @GetMapping("/findAll")
    public List<User> findAll(){

        return userService.findAll();
    }

    /**
     * 需求: http://localhost:8090/findUserById?id=1
     * 参数接收: id=1
     * 返回值: User对象
     */
    @GetMapping("/findUserById")
    public User findUserById(Integer id){

        return userService.findUserById(id);
    }

    /**
     * URL地址: http://localhost:8090/findUserByAS?age=18&sex=女
     * 参数:  age=18  sex=女
     * 返回值: List<集合>
     * 参数接收: 如果是多个参数,并且参数名称与属性名称一致,可以使用对象接收
     */
        @GetMapping("/findUserByAS")
        public List<User> findUserByAS(User user){

            return userService.findUserByAS(user);
    }

    /**
     * URL:  http://localhost:8090/updateById/1/黑熊精/3000/男
     * 参数: 4个
     * 返回值: "修改成功!!!!"
     * restFul结构 参数分析 {属性名称}
     * 参数接收:
     *      1. 单个参数使用 @PathVariable Integer id接收
     *      2. 如果多个参数接收 使用对象 mvc自动提供的功能.
     */
    /*@RequestMapping(value = "/updateById/{id}/{name}/{age}/{sex}",
                    method = RequestMethod.PUT)*/
    @PutMapping("/updateById/{id}/{name}/{age}/{sex}")
    public String updateById(User user){

        userService.updateById(user);
        return "用户修改成功";
    }


    /**
     * 模糊查询:
     *      url:http://localhost:8090/findUserByLike?name=乔
     *      参数: name = "乔"
     *      返回值: List<User>
     */
    @GetMapping("/findUserByLike")
    public List<User> findUserByLike(String name){

        return userService.findUserByLike(name);
    }


    /**
     * 查询id=1,3,5,7的数据
     * URL: http://localhost:8090/findUserByIds?ids=1,3,5,7
     * 参数: ids=1,3,5,7
     * 返回值: List<User>
     * 知识点: MVC中参数,号分割的规则. 应该使用数组接收 mvc的规则
     */
    @GetMapping("/findUserByIds")
    public List<User> findUserByIds(Integer[] ids){

        return userService.findUserByIds(ids);
    }

    /**
     * 完成用户入库操作
     * http://localhost:8090/saveUser/悟空/8000/男     返回成功的提示即可
     * 参数: 3个
     * 返回值: String
     */
    @RequestMapping("/saveUser/{name}/{age}/{sex}")
    public String saveUser(User user){

        userService.saveUser(user);
        return "新增入库成功!!!";
    }


}
