package com.jt.mapper;

import com.jt.pojo.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
//@Mapper //将Mapper接口交给Spring容器管理.
public interface UserMapper {

    List<User> findAll();

    //注解和映射文件只能二选一
    @Select("select * from demo_user where id = #{id}")
    User findUserById(Integer id);

    @Select("select * from demo_user where age=#{age} and sex=#{sex}")
    List<User> findUserByAS(User user);
    @Update("update demo_user set name=#{name},age=#{age}," +
            "sex =#{sex} where id=#{id}")
    void updateById(User user);

    @Select("select * from demo_user where name like \"%\"#{name}\"%\" ")
    List<User> findUserByLike(String name);

    List<User> findUserByIds(@Param("ids") Integer[] ids);

    @Insert("insert into demo_user(id,name,age,sex) value(null,#{name},#{age},#{sex})")
    void saveUser(User user);
}
