package com.jt;

import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class SpringbootSsmApplicationTests {

    @Autowired  //注入指定的对象
    private UserMapper userMapper;
    //class com.sun.proxy.$Proxy67 代理对象 spring容器自动创建的代理对象
    //IDEA 编译提示 不影响执行

    /**
     * 测试SpringBoot整合Mybatis
     */
    @Test
    public void testfindAll(){
        List<User> userList = userMapper.findAll();
        System.out.println(userMapper.getClass());
        System.out.println(userList);
    }
}
